package com.devcamp.exportexcel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.exportexcel.model.CMenu;

public interface IMenuRepository extends JpaRepository<CMenu, Long> {

}
